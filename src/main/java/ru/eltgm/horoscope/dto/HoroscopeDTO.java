package ru.eltgm.horoscope.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HoroscopeDTO {
    private String zodiacSign;
    private String type;
    private String text;
    private String week;
    private String date;
    private String id;
}
