package ru.eltgm.horoscope.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import ru.eltgm.horoscope.models.Type;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TypeDto {
    private String id;
    private String typeName;
    private String typeNameRussian;
    private String isPremium;
    private String dayType;

    public Type toPojo() {
        return Type.builder()
                .id(this.id == null ? null : new ObjectId(this.id))
                .typeName(this.typeName)
                .typeNameRussian(this.typeNameRussian)
                .dayType(this.dayType)
                .isPremium(this.isPremium == null ? Boolean.FALSE : Boolean.valueOf(this.isPremium))
                .build();
    }
}
