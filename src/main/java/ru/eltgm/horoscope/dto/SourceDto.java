package ru.eltgm.horoscope.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import ru.eltgm.horoscope.models.HoroscopeSource;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SourceDto {
    private String id;
    private String type;
    private String horoscopeType;
    private String url;
    private String isActive;

    public HoroscopeSource toPojo() {
        return HoroscopeSource.builder()
                .id(this.id == null ? null : new ObjectId(this.id))
                .type(this.type)
                .horoscopeType(this.horoscopeType)
                .isActive(this.isActive == null ? Boolean.FALSE : Boolean.valueOf(this.isActive))
                .url(this.url)
                .build();
    }
}
