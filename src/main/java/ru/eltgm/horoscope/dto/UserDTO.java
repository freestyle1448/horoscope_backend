package ru.eltgm.horoscope.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {
    private String id;
    private String login;
    private String isPremium;
    private String birthDate;
    private String zodiacSign;

    private String password;
    private String passwordConfirm;
}
