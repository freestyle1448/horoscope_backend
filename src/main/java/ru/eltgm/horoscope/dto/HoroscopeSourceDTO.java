package ru.eltgm.horoscope.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.eltgm.horoscope.models.HoroscopeSource;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HoroscopeSourceDTO {
    private String id;
    private String type;
    private String horoscopeType;
    private String url;
    private String isActive;

    public HoroscopeSource toPojo() {
        return HoroscopeSource.builder()
                .horoscopeType(this.horoscopeType)
                .isActive(this.isActive == null ? Boolean.FALSE : Boolean.valueOf(this.isActive))
                .type(this.type)
                .url(this.url)
                .build();
    }
}
