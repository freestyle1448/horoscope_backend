package ru.eltgm.horoscope.tasks;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.eltgm.horoscope.repository.HoroscopeSourcesRepository;
import ru.eltgm.horoscope.service.HoroscopeService;

@Component
@RequiredArgsConstructor
public class UpdateHoroscopes {
    private final HoroscopeService horoscopeService;
    private final HoroscopeSourcesRepository sourcesRepository;

    //@Scheduled(fixedDelay = 1200000)
    //@Scheduled(cron = "${update.day.cron}")
    public void updateDayHoroscope() {
        final var allActiveDayHoroscopeSources = sourcesRepository.findAllActiveDayHoroscopeSources();

        horoscopeService.deleteOldDay();
        allActiveDayHoroscopeSources.forEach(horoscopeService::updateDay);
    }

    //@Scheduled(fixedDelay = 1200000)
    //@Scheduled(cron = "${update.weekly.cron}")
    public void updateWeeklyHoroscope() {
        final var allActiveWeeklyHoroscopeSources = sourcesRepository.findAllActiveWeeklyHoroscopeSources();

        horoscopeService.deleteOldWeek();
        allActiveWeeklyHoroscopeSources.forEach(horoscopeService::updateWeekly);
    }
}
