package ru.eltgm.horoscope.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.eltgm.horoscope.dto.UserDTO;
import ru.eltgm.horoscope.exception.RegistrationException;
import ru.eltgm.horoscope.models.Role;
import ru.eltgm.horoscope.models.User;
import ru.eltgm.horoscope.service.UserService;

import java.text.SimpleDateFormat;
import java.util.Date;

@RequiredArgsConstructor
@RestController
public class SecurityControllerImpl implements SecurityController {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
    private final UserService userService;

    @PostMapping("/api/security/auth")
    @Override
    public Boolean authorization(UserDTO userDto) {
        return userService.isExists(userDto.getLogin(), userDto.getPassword());
    }

    @PostMapping("/api/security/user/reg")
    @SneakyThrows
    @Override
    public Boolean registrationUser(UserDTO userDto) {
        if (!userDto.getPassword().equals(userDto.getPasswordConfirm())) {
            throw new RegistrationException("Пароли не совпадают");
        }
        if (!userService.saveUser(User.builder()
                .password(userDto.getPassword())
                .login(userDto.getLogin())
                .isPremium(Boolean.valueOf(userDto.getIsPremium()))
                .regDate(new Date())
                .zodiacSign(userDto.getZodiacSign())
                .birthDate(dateFormat.parse(userDto.getBirthDate()))
                .role(Role.builder()
                        .roleName("ROLE_USER")
                        .build())
                .build())) {
            throw new RegistrationException("Пользователь с таким именем уже существует");
        }
        return true;
    }

    @PostMapping("/api/security/admin/reg")
    @SneakyThrows
    @Override
    public Boolean registrationAdmin(UserDTO userDto) {
        if (!userDto.getPassword().equals(userDto.getPasswordConfirm())) {
            throw new RegistrationException("Пароли не совпадают");
        }
        if (!userService.saveUser(User.builder()
                .password(userDto.getPassword())
                .login(userDto.getLogin())
                .regDate(new Date())
                .role(Role.builder()
                        .roleName("ROLE_ADMIN")
                        .build())
                .build())) {
            throw new RegistrationException("Пользователь с таким именем уже существует");
        }

        return true;
    }
}
