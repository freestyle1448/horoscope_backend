package ru.eltgm.horoscope.controller;

import ru.eltgm.horoscope.dto.UserDTO;

public interface SecurityController {
    Boolean authorization(UserDTO userDto);

    Boolean registrationUser(UserDTO userDto);

    Boolean registrationAdmin(UserDTO userDto);
}
