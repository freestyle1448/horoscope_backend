package ru.eltgm.horoscope.controller;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import ru.eltgm.horoscope.dto.*;
import ru.eltgm.horoscope.exception.NotFoundException;
import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.HoroscopeSource;
import ru.eltgm.horoscope.models.Type;
import ru.eltgm.horoscope.models.User;
import ru.eltgm.horoscope.service.AdminService;

import java.text.SimpleDateFormat;
import java.util.List;


@CrossOrigin
@RestController
@RequiredArgsConstructor
public class AdminControllerImpl implements AdminController {
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private final AdminService adminService;

    @SneakyThrows
    @PostMapping("/api/admin/horoscope")
    public Boolean createHoroscope(HoroscopeDTO horoscope) {
        if (horoscope != null)
            return adminService.saveHoroscope(Horoscope.builder()
                    .id(horoscope.getId() == null ? null : new ObjectId(horoscope.getId()))
                    .text(horoscope.getText())
                    .zodiacSign(horoscope.getZodiacSign())
                    .type(horoscope.getType())
                    .week(horoscope.getWeek())
                    .date(horoscope.getDate())
                    .build());

        return false;
    }

    @GetMapping("/api/admin/horoscope/{date}/{sign}/{type}")
    public Page<Horoscope> getHoroscopes(
            @PathVariable String date,
            @PathVariable String sign,
            @PathVariable String type,
            Integer page) {
        Page<Horoscope> horoscopes = null;

        if (date.equals("week")) {
            horoscopes = adminService.getWeeklyHoroscopes(sign, type, page);
        }
        if (date.equals("day")) {
            horoscopes = adminService.getDayHoroscopes(sign, type, page);
        }

        return horoscopes;
    }

    @GetMapping("/api/admin/type/{page}")
    public Page<Type> getTypes(@PathVariable("page") Integer page) {
        if (page != null) {
            return adminService.getTypes(page);
        }

        return null;
    }

    @Override
    @GetMapping("/api/admin/type")
    public List<Type> getAllTypes() {
        return adminService.getAllTypes();
    }

    @DeleteMapping("/api/admin/type/{id}")
    public void deleteType(@PathVariable("id") String id) {
        adminService.deleteType(new ObjectId(id));
    }

    @PostMapping("/api/admin/type")
    public void createType(@RequestBody TypeDto type) {
        adminService.createType(type.toPojo());
    }

    @PutMapping("/api/admin/type")
    public void updateType(@RequestBody TypeDto type) {
        if (type.getId() == null) {
            throw new NotFoundException("Невозможно обновить! Нет ID типа");
        }

        adminService.updateType(type.toPojo());
    }

    @GetMapping("/api/admin/user")
    public Page<User> getUsers(Integer page) {
        if (page != null) {
            return adminService.getUsers(page);
        }

        return null;
    }

    @GetMapping("/api/admin/source")
    public Page<HoroscopeSource> getSources(Integer page) {
        if (page != null) {
            return adminService.getSources(page);
        }

        return null;
    }

    @Override
    @PutMapping("/api/admin/source")
    public Boolean updateSource(@RequestBody SourceDto sourceDto) {
        if (sourceDto.getId() == null) {
            return false;
        }

        return adminService.updateSource(sourceDto.toPojo());
    }

    @Override
    @DeleteMapping("/api/admin/source/{id}")
    public Boolean deleteSource(@PathVariable("id") String id) {
        return adminService.deleteSource(new ObjectId(id));
    }

    @SneakyThrows
    @PostMapping("/api/admin/user")
    public Boolean saveUser(UserDTO userDTO) {
        User user;
        if (userDTO.getId() != null) {
            user = User.builder()
                    .id(new ObjectId(userDTO.getId()))
                    .birthDate(dateFormat.parse(userDTO.getBirthDate()))
                    .isPremium(Boolean.valueOf(userDTO.getIsPremium()))
                    .login(userDTO.getLogin())
                    .zodiacSign(userDTO.getZodiacSign())
                    .build();
            return adminService.saveUser(user);
        }

        return false;
    }

    @PostMapping("/api/admin/source")
    public Boolean createSource(@RequestBody HoroscopeSourceDTO horoscopeSourceDTO) {
        return adminService.saveSource(horoscopeSourceDTO.toPojo());
    }
}