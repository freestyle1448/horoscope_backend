package ru.eltgm.horoscope.controller;

import org.springframework.data.domain.Page;
import ru.eltgm.horoscope.dto.HoroscopeDTO;
import ru.eltgm.horoscope.dto.HoroscopeSourceDTO;
import ru.eltgm.horoscope.dto.SourceDto;
import ru.eltgm.horoscope.dto.UserDTO;
import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.HoroscopeSource;
import ru.eltgm.horoscope.models.Type;
import ru.eltgm.horoscope.models.User;

import java.util.List;

public interface AdminController {
    Boolean createHoroscope(HoroscopeDTO horoscope);

    Page<Horoscope> getHoroscopes(String date, String sign, String type, Integer page);

    Page<Type> getTypes(Integer page);

    List<Type> getAllTypes();

    Page<User> getUsers(Integer page);

    Boolean saveUser(UserDTO userDTO);

    Page<HoroscopeSource> getSources(Integer page);

    Boolean updateSource(SourceDto sourceDto);

    Boolean deleteSource(String id);

    Boolean createSource(HoroscopeSourceDTO horoscopeSourceDTO);
}
