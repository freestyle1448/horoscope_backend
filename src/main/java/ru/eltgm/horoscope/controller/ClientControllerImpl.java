package ru.eltgm.horoscope.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.UserType;
import ru.eltgm.horoscope.repository.UsersRepository;
import ru.eltgm.horoscope.service.ClientService;

import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor
@RestController
public class ClientControllerImpl implements ClientController {
    private final ClientService clientService;
    private final UsersRepository usersRepository;

    @Override
    @GetMapping("/api/user/type/{dayType}")
    public List<UserType> getAllTypesByDayType(@PathVariable(name = "dayType") String dayType) {
        final var principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final var user = usersRepository.findByLogin(principal.getUsername());

        if (user != null) {
            return clientService.getAllTypesByDayType(user, dayType);
        }

        return Collections.emptyList();
    }

    @Override
    @GetMapping("/api/user/horoscope/{zodiacSign}")
    public List<Horoscope> getHoroscopesBySign(@PathVariable("zodiacSign") String zodiacSign) {
        final var principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final var user = usersRepository.findByLogin(principal.getUsername());

        if (user != null) {
            return clientService.getAllAvailableHoroscopesBySign(user, zodiacSign);
        }

        return Collections.emptyList();
    }

    @Override
    @GetMapping("/api/user/horoscope")
    public List<Horoscope> getHoroscopes() {
        final var principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final var user = usersRepository.findByLogin(principal.getUsername());

        if (user != null) {
            return clientService.getAllAvailableHoroscopes(user);
        }

        return Collections.emptyList();
    }

    @Override
    @GetMapping("/api/user/horoscope/{zodiacSign}/{day}/{horoscopeType}")
    public Horoscope getHoroscopeBySignDayType(@PathVariable("zodiacSign") String zodiacSign
            , @PathVariable("day") String day, @PathVariable("horoscopeType") String horoscopeType) {
        final var principal = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        final var user = usersRepository.findByLogin(principal.getUsername());

        if (user != null) {
            return clientService.getHoroscopeBySignDayType(user, zodiacSign, day, horoscopeType);
        }

        return new Horoscope();
    }
}
