package ru.eltgm.horoscope.controller;

import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.UserType;

import java.util.List;

public interface ClientController {
    List<UserType> getAllTypesByDayType(String dayType);

    List<Horoscope> getHoroscopesBySign(String zodiacSign);

    List<Horoscope> getHoroscopes();

    Horoscope getHoroscopeBySignDayType(String zodiacSign
            , String day, String horoscopeType);
}
