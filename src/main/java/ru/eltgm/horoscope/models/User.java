package ru.eltgm.horoscope.models;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder

@Document(collection = "users")
public class User {
    @Id
    private ObjectId id;
    private String login;
    private Date regDate;
    private Boolean isPremium;
    private Date birthDate;
    private String zodiacSign;
    private String password;
    private Role role;
}
