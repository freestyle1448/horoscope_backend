package ru.eltgm.horoscope.models.ignio.day;

import lombok.Value;
import ru.eltgm.horoscope.models.ignio.*;

@Value
public class HoroDay {
    HoroscopeDate date;
    Aries aries;
    Taurus taurus;
    Gemini gemini;
    Cancer cancer;
    Leo leo;
    Virgo virgo;
    Libra libra;
    Scorpio scorpio;
    Sagittarius sagittarius;
    Capricorn capricorn;
    Aquarius aquarius;
    Pisces pisces;
}
