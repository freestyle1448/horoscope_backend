package ru.eltgm.horoscope.models.ignio.weekly;

import lombok.Value;
import ru.eltgm.horoscope.models.ignio.*;

import javax.xml.bind.annotation.XmlRootElement;

@Value
@XmlRootElement(name = "horo")
public class HoroWeekly {
    HoroscopeDate date;
    Aries aries;
    Taurus taurus;
    Gemini gemini;
    Cancer cancer;
    Leo leo;
    Virgo virgo;
    Libra libra;
    Scorpio scorpio;
    Sagittarius sagittarius;
    Capricorn capricorn;
    Aquarius aquarius;
    Pisces pisces;
}
