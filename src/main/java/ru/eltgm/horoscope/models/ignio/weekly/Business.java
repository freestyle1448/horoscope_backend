package ru.eltgm.horoscope.models.ignio.weekly;

import lombok.Value;

@Value
public class Business {
    String text;
}
