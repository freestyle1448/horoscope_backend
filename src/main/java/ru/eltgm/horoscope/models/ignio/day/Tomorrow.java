package ru.eltgm.horoscope.models.ignio.day;

import lombok.Value;

@Value
public class Tomorrow {
    String text;
}
