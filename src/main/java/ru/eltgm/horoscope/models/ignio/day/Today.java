package ru.eltgm.horoscope.models.ignio.day;

import lombok.Value;

@Value
public class Today {
    String text;
}
