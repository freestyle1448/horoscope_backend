package ru.eltgm.horoscope.models.ignio;

import lombok.Value;
import ru.eltgm.horoscope.models.ignio.day.Today;
import ru.eltgm.horoscope.models.ignio.day.Tomorrow;
import ru.eltgm.horoscope.models.ignio.day.Tomorrow02;
import ru.eltgm.horoscope.models.ignio.day.Yesterday;
import ru.eltgm.horoscope.models.ignio.weekly.*;

@Value
public class Taurus {
    Yesterday yesterday;
    Today today;
    Tomorrow tomorrow;
    Tomorrow02 tomorrow02;

    Business business;
    Common common;
    Love love;
    Health health;
    Car car;
    Beauty beauty;
    Erotic erotic;
    Gold gold;
}
