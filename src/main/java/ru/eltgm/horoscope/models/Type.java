package ru.eltgm.horoscope.models;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder

@Document("types")
public class Type {
    @Id
    private ObjectId id;
    private String typeName;
    private Boolean isPremium;
    private String dayType;
    private String typeNameRussian;
}
