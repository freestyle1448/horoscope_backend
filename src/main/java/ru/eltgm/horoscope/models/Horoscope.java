package ru.eltgm.horoscope.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "horoscopes")
public class Horoscope {
    @Id
    private ObjectId id;
    private String date;
    private String zodiacSign;
    private String type;
    private String week;
    private String text;
}
