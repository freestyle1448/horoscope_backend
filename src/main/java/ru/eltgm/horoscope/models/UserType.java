package ru.eltgm.horoscope.models;

import lombok.Value;

@Value
public class UserType {
    String typeName;
    Boolean isAvailable;
    String typeNameRussian;
    String dayType;
}
