package ru.eltgm.horoscope.models;

import lombok.Builder;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder

@Document(collection = "admins")
public class Admin {
    @Id
    private ObjectId id;
    private String login;
    private String password;
}
