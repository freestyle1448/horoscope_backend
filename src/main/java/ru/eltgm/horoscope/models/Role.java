package ru.eltgm.horoscope.models;

import lombok.Builder;
import org.springframework.security.core.GrantedAuthority;

@Builder
public class Role implements GrantedAuthority {
    private final String roleName;

    @Override
    public String getAuthority() {
        return roleName;
    }
}
