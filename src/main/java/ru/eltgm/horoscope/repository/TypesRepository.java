package ru.eltgm.horoscope.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.eltgm.horoscope.models.Type;

import java.util.List;

public interface TypesRepository extends MongoRepository<Type, ObjectId> {
    List<Type> findAllByIsPremiumFalse();

    @Query("{" +
            " $or : " +
            "[" +
            "{'dayType': 'all'}, " +
            "{'dayType': :#{#dayType} }" +
            "]" +
            "}")
    List<Type> findAllByDayType(@Param("dayType") String dayType);
}
