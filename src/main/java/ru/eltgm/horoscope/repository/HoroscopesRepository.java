package ru.eltgm.horoscope.repository;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import ru.eltgm.horoscope.models.Horoscope;

import java.util.List;

public interface HoroscopesRepository extends MongoRepository<Horoscope, ObjectId> {
    Horoscope findByZodiacSignAndDateAndType(String sign, String date, String horoscopeType);

    Horoscope findByZodiacSignAndWeekIsNotNullAndType(String sign, String horoscopeType);

    List<Horoscope> findAllByTypeInAndZodiacSign(List<String> types, String sign);

    List<Horoscope> findAllByTypeIn(List<String> types);

    List<Horoscope> deleteAllByDateNotNullAndWeekNull();

    List<Horoscope> deleteAllByWeekNotNullAndDateNull();

    Page<Horoscope> findAllByZodiacSignAndTypeAndDateNotNullAndWeekNull(String zodiacSign, String type, Pageable pageRequest);

    Page<Horoscope> findAllByZodiacSignAndTypeAndWeekNotNullAndDateNull(String zodiacSign, String type, Pageable pageRequest);
}
