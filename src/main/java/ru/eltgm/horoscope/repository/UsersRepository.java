package ru.eltgm.horoscope.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import ru.eltgm.horoscope.models.User;

public interface UsersRepository extends MongoRepository<User, ObjectId> {
    User findByLogin(String login);
}
