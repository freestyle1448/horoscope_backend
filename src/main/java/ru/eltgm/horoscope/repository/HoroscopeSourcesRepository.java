package ru.eltgm.horoscope.repository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import ru.eltgm.horoscope.models.HoroscopeSource;

import java.util.List;

public interface HoroscopeSourcesRepository extends MongoRepository<HoroscopeSource, ObjectId> {
    @Query("{'isActive' : true, 'type' : 'weekly'}")
    List<HoroscopeSource> findAllActiveWeeklyHoroscopeSources();

    @Query("{'isActive' : true, 'type' : 'day'}")
    List<HoroscopeSource> findAllActiveDayHoroscopeSources();
}
