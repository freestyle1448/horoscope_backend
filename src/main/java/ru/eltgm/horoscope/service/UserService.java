package ru.eltgm.horoscope.service;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import ru.eltgm.horoscope.repository.UsersRepository;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
    private final UsersRepository usersRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final var user = usersRepository.findByLogin(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return new User(user.getLogin(), user.getPassword(),
                Collections.singleton(new SimpleGrantedAuthority(user.getRole().getAuthority())));
    }

    public boolean isExists(String login, String password) {
        final var userDetails = loadUserByUsername(login);
        return bCryptPasswordEncoder.matches(password, userDetails.getPassword());
    }

    public boolean saveUser(ru.eltgm.horoscope.models.User user) {
        var userFromDB = usersRepository.findByLogin(user.getLogin());

        if (userFromDB != null) {
            return false;
        }
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        usersRepository.save(user);
        return true;
    }

    public boolean deleteUser(String userId) {
        if (usersRepository.findById(new ObjectId(userId)).isPresent()) {
            usersRepository.deleteById(new ObjectId(userId));
            return true;
        }
        return false;
    }
}
