package ru.eltgm.horoscope.service.ignio;

import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.ignio.day.HoroDay;
import ru.eltgm.horoscope.models.ignio.weekly.HoroWeekly;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("Duplicates")
public class HoroscopeMapper {
    public static List<Horoscope> getHoroscopes(HoroDay horoDay, String horoscopeType) {
        final var horoscopes = new ArrayList<Horoscope>();
        final var dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        final var today = dateFormat.format(horoDay.getDate().getToday());
        final var tomorrow = dateFormat.format(horoDay.getDate().getTomorrow());
        final var tomorrow2 = dateFormat.format(horoDay.getDate().getTomorrow02());

        final var aquarius = horoDay.getAquarius();
        final var aries = horoDay.getAries();
        final var taurus = horoDay.getTaurus();
        final var gemini = horoDay.getGemini();
        final var cancer = horoDay.getCancer();
        final var leo = horoDay.getLeo();
        final var virgo = horoDay.getVirgo();
        final var libra = horoDay.getLibra();
        final var scorpio = horoDay.getScorpio();
        final var sagittarius = horoDay.getSagittarius();
        final var capricorn = horoDay.getCapricorn();
        final var pisces = horoDay.getPisces();

        final var todayAquarius = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("aquarius")
                .text(aquarius.getToday().getText().replace("\n", ""))
                .build();
        final var todayAries = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("aries")
                .text(aries.getToday().getText().replace("\n", ""))
                .build();
        final var todayTaurus = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("taurus")
                .text(taurus.getToday().getText().replace("\n", ""))
                .build();
        final var todayGemini = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("gemini")
                .text(gemini.getToday().getText().replace("\n", ""))
                .build();
        final var todayCancer = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("cancer")
                .text(cancer.getToday().getText().replace("\n", ""))
                .build();
        final var todayLeo = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("leo")
                .text(leo.getToday().getText().replace("\n", ""))
                .build();
        final var todayVirgo = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("virgo")
                .text(virgo.getToday().getText().replace("\n", ""))
                .build();
        final var todayLibra = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("libra")
                .text(libra.getToday().getText().replace("\n", ""))
                .build();
        final var todayScorpio = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("scorpio")
                .text(scorpio.getToday().getText().replace("\n", ""))
                .build();
        final var todaySagittarius = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("sagittarius")
                .text(sagittarius.getToday().getText().replace("\n", ""))
                .build();
        final var todayCapricorn = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("capricorn")
                .text(capricorn.getToday().getText().replace("\n", ""))
                .build();
        final var todayPisces = Horoscope.builder()
                .date(today)
                .type(horoscopeType)
                .zodiacSign("pisces")
                .text(pisces.getToday().getText().replace("\n", ""))
                .build();


        final var tomorrowAquarius = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("aquarius")
                .text(aquarius.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowAries = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("aries")
                .text(aries.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowTaurus = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("taurus")
                .text(taurus.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowGemini = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("gemini")
                .text(gemini.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowCancer = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("cancer")
                .text(cancer.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowLeo = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("leo")
                .text(leo.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowVirgo = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("virgo")
                .text(virgo.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowLibra = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("libra")
                .text(libra.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowScorpio = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("scorpio")
                .text(scorpio.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowSagittarius = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("sagittarius")
                .text(sagittarius.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowCapricorn = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("capricorn")
                .text(capricorn.getTomorrow().getText().replace("\n", ""))
                .build();
        final var tomorrowPisces = Horoscope.builder()
                .date(tomorrow)
                .type(horoscopeType)
                .zodiacSign("pisces")
                .text(pisces.getTomorrow().getText().replace("\n", ""))
                .build();


        final var tomorrow2Aquarius = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("aquarius")
                .text(aquarius.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Aries = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("aries")
                .text(aries.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Taurus = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("taurus")
                .text(taurus.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Gemini = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("gemini")
                .text(gemini.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Cancer = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("cancer")
                .text(cancer.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Leo = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("leo")
                .text(leo.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Virgo = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("virgo")
                .text(virgo.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Libra = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("libra")
                .text(libra.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Scorpio = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("scorpio")
                .text(scorpio.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Sagittarius = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("sagittarius")
                .text(sagittarius.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Capricorn = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("capricorn")
                .text(capricorn.getTomorrow02().getText().replace("\n", ""))
                .build();
        final var tomorrow2Pisces = Horoscope.builder()
                .date(tomorrow2)
                .type(horoscopeType)
                .zodiacSign("pisces")
                .text(pisces.getTomorrow02().getText().replace("\n", ""))
                .build();

        Collections.addAll(horoscopes,
                todayAries, todayTaurus, todayGemini,
                todayCancer, todayLeo, todayVirgo,
                todayLibra, todayScorpio, todaySagittarius,
                todayCapricorn, todayAquarius, todayPisces,

                tomorrowAries, tomorrowTaurus, tomorrowGemini,
                tomorrowCancer, tomorrowLeo, tomorrowVirgo,
                tomorrowLibra, tomorrowScorpio, tomorrowSagittarius,
                tomorrowCapricorn, tomorrowAquarius, tomorrowPisces,

                tomorrow2Aries, tomorrow2Taurus, tomorrow2Gemini,
                tomorrow2Cancer, tomorrow2Leo, tomorrow2Virgo,
                tomorrow2Libra, tomorrow2Scorpio, tomorrow2Sagittarius,
                tomorrow2Capricorn, tomorrow2Aquarius, tomorrow2Pisces);

        return horoscopes;
    }

    public static List<Horoscope> getWeeklyHoroscopes(HoroWeekly horoWeekly) {
        final var horoscopes = new ArrayList<Horoscope>();

        final var week = horoWeekly.getDate().getWeekly();

        final var cancer = horoWeekly.getCancer();
        final var leo = horoWeekly.getLeo();
        final var virgo = horoWeekly.getVirgo();

        final var cancerCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("cancer")
                .text(cancer.getCommon().getText().replace("\n", ""))
                .build();
        final var cancerBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("cancer")
                .text(cancer.getBusiness().getText().replace("\n", ""))
                .build();
        final var cancerLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("cancer")
                .text(cancer.getLove().getText().replace("\n", ""))
                .build();
        final var cancerCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("cancer")
                .text(cancer.getCar().getText().replace("\n", ""))
                .build();
        final var cancerHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("cancer")
                .text(cancer.getHealth().getText().replace("\n", ""))
                .build();
        final var cancerBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("cancer")
                .text(cancer.getBeauty().getText().replace("\n", ""))
                .build();
        final var cancerErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("cancer")
                .text(cancer.getErotic().getText().replace("\n", ""))
                .build();
        final var cancerGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("cancer")
                .text(cancer.getGold().getText().replace("\n", ""))
                .build();


        final var leoCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("leo")
                .text(leo.getCommon().getText().replace("\n", ""))
                .build();
        final var leoBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("leo")
                .text(leo.getBusiness().getText().replace("\n", ""))
                .build();
        final var leoLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("leo")
                .text(leo.getLove().getText().replace("\n", ""))
                .build();
        final var leoCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("leo")
                .text(leo.getCar().getText().replace("\n", ""))
                .build();
        final var leoHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("leo")
                .text(leo.getHealth().getText().replace("\n", ""))
                .build();
        final var leoBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("leo")
                .text(leo.getBeauty().getText().replace("\n", ""))
                .build();
        final var leoErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("leo")
                .text(leo.getErotic().getText().replace("\n", ""))
                .build();
        final var leoGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("leo")
                .text(leo.getGold().getText().replace("\n", ""))
                .build();


        final var virgoCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("virgo")
                .text(virgo.getCommon().getText().replace("\n", ""))
                .build();
        final var virgoBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("virgo")
                .text(virgo.getBusiness().getText().replace("\n", ""))
                .build();
        final var virgoLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("virgo")
                .text(virgo.getLove().getText().replace("\n", ""))
                .build();
        final var virgoCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("virgo")
                .text(virgo.getCar().getText().replace("\n", ""))
                .build();
        final var virgoHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("virgo")
                .text(virgo.getHealth().getText().replace("\n", ""))
                .build();
        final var virgoBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("virgo")
                .text(virgo.getBeauty().getText().replace("\n", ""))
                .build();
        final var virgoErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("virgo")
                .text(virgo.getErotic().getText().replace("\n", ""))
                .build();
        final var virgoGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("virgo")
                .text(virgo.getGold().getText().replace("\n", ""))
                .build();

        Collections.addAll(horoscopes,
                cancerBusiness, cancerCommon, cancerLove, cancerCar, cancerHealth, cancerBeauty, cancerErotic, cancerGold,
                leoBusiness, leoCommon, leoLove, leoCar, leoHealth, leoBeauty, leoErotic, leoGold,
                virgoBusiness, virgoCommon, virgoLove, virgoCar, virgoHealth, virgoBeauty, virgoErotic, virgoGold);


        final var libra = horoWeekly.getLibra();
        final var scorpio = horoWeekly.getScorpio();
        final var sagittarius = horoWeekly.getSagittarius();

        final var libraCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("libra")
                .text(libra.getCommon().getText().replace("\n", ""))
                .build();
        final var libraBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("libra")
                .text(libra.getBusiness().getText().replace("\n", ""))
                .build();
        final var libraLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("libra")
                .text(libra.getLove().getText().replace("\n", ""))
                .build();
        final var libraCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("libra")
                .text(libra.getCar().getText().replace("\n", ""))
                .build();
        final var libraHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("libra")
                .text(libra.getHealth().getText().replace("\n", ""))
                .build();
        final var libraBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("libra")
                .text(libra.getBeauty().getText().replace("\n", ""))
                .build();
        final var libraErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("libra")
                .text(libra.getErotic().getText().replace("\n", ""))
                .build();
        final var libraGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("libra")
                .text(libra.getGold().getText().replace("\n", ""))
                .build();


        final var scorpioCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("scorpio")
                .text(scorpio.getCommon().getText().replace("\n", ""))
                .build();
        final var scorpioBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("scorpio")
                .text(scorpio.getBusiness().getText().replace("\n", ""))
                .build();
        final var scorpioLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("scorpio")
                .text(scorpio.getLove().getText().replace("\n", ""))
                .build();
        final var scorpioCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("scorpio")
                .text(scorpio.getCar().getText().replace("\n", ""))
                .build();
        final var scorpioHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("scorpio")
                .text(scorpio.getHealth().getText().replace("\n", ""))
                .build();
        final var scorpioBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("scorpio")
                .text(scorpio.getBeauty().getText().replace("\n", ""))
                .build();
        final var scorpioErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("scorpio")
                .text(scorpio.getErotic().getText().replace("\n", ""))
                .build();
        final var scorpioGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("scorpio")
                .text(scorpio.getGold().getText().replace("\n", ""))
                .build();


        final var sagittariusCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("sagittarius")
                .text(sagittarius.getCommon().getText().replace("\n", ""))
                .build();
        final var sagittariusBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("sagittarius")
                .text(sagittarius.getBusiness().getText().replace("\n", ""))
                .build();
        final var sagittariusLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("sagittarius")
                .text(sagittarius.getLove().getText().replace("\n", ""))
                .build();
        final var sagittariusCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("sagittarius")
                .text(sagittarius.getCar().getText().replace("\n", ""))
                .build();
        final var sagittariusHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("sagittarius")
                .text(sagittarius.getHealth().getText().replace("\n", ""))
                .build();
        final var sagittariusBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("sagittarius")
                .text(sagittarius.getBeauty().getText().replace("\n", ""))
                .build();
        final var sagittariusErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("sagittarius")
                .text(sagittarius.getErotic().getText().replace("\n", ""))
                .build();
        final var sagittariusGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("sagittarius")
                .text(sagittarius.getGold().getText().replace("\n", ""))
                .build();

        Collections.addAll(horoscopes,
                libraBusiness, libraCommon, libraLove, libraCar, libraHealth, libraBeauty, libraErotic, libraGold,
                scorpioBusiness, scorpioCommon, scorpioLove, scorpioCar, scorpioHealth, scorpioBeauty, scorpioErotic, scorpioGold,
                sagittariusBusiness, sagittariusCommon, sagittariusLove, sagittariusCar, sagittariusHealth, sagittariusBeauty, sagittariusErotic, sagittariusGold);


        final var capricorn = horoWeekly.getCapricorn();
        final var pisces = horoWeekly.getPisces();
        final var aquarius = horoWeekly.getAquarius();

        final var capricornCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("capricorn")
                .text(capricorn.getCommon().getText().replace("\n", ""))
                .build();
        final var capricornBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("capricorn")
                .text(capricorn.getBusiness().getText().replace("\n", ""))
                .build();
        final var capricornLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("capricorn")
                .text(capricorn.getLove().getText().replace("\n", ""))
                .build();
        final var capricornCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("capricorn")
                .text(capricorn.getCar().getText().replace("\n", ""))
                .build();
        final var capricornHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("capricorn")
                .text(capricorn.getHealth().getText().replace("\n", ""))
                .build();
        final var capricornBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("capricorn")
                .text(capricorn.getBeauty().getText().replace("\n", ""))
                .build();
        final var capricornErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("capricorn")
                .text(capricorn.getErotic().getText().replace("\n", ""))
                .build();
        final var capricornGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("capricorn")
                .text(capricorn.getGold().getText().replace("\n", ""))
                .build();


        final var aquariusCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("aquarius")
                .text(aquarius.getCommon().getText().replace("\n", ""))
                .build();
        final var aquariusBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("aquarius")
                .text(aquarius.getBusiness().getText().replace("\n", ""))
                .build();
        final var aquariusLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("aquarius")
                .text(aquarius.getLove().getText().replace("\n", ""))
                .build();
        final var aquariusCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("aquarius")
                .text(aquarius.getCar().getText().replace("\n", ""))
                .build();
        final var aquariusHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("aquarius")
                .text(aquarius.getHealth().getText().replace("\n", ""))
                .build();
        final var aquariusBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("aquarius")
                .text(aquarius.getBeauty().getText().replace("\n", ""))
                .build();
        final var aquariusErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("aquarius")
                .text(aquarius.getErotic().getText().replace("\n", ""))
                .build();
        final var aquariusGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("aquarius")
                .text(aquarius.getGold().getText().replace("\n", ""))
                .build();

        final var piscesCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("pisces")
                .text(pisces.getCommon().getText().replace("\n", ""))
                .build();
        final var piscesBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("pisces")
                .text(pisces.getBusiness().getText().replace("\n", ""))
                .build();
        final var piscesLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("pisces")
                .text(pisces.getLove().getText().replace("\n", ""))
                .build();
        final var piscesCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("pisces")
                .text(pisces.getCar().getText().replace("\n", ""))
                .build();
        final var piscesHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("pisces")
                .text(pisces.getHealth().getText().replace("\n", ""))
                .build();
        final var piscesBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("pisces")
                .text(pisces.getBeauty().getText().replace("\n", ""))
                .build();
        final var piscesErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("pisces")
                .text(pisces.getErotic().getText().replace("\n", ""))
                .build();
        final var piscesGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("pisces")
                .text(pisces.getGold().getText().replace("\n", ""))
                .build();

        Collections.addAll(horoscopes,
                capricornBusiness, capricornCommon, capricornLove, capricornCar, capricornHealth, capricornBeauty, capricornErotic, capricornGold,
                aquariusBusiness, aquariusCommon, aquariusLove, aquariusCar, aquariusHealth, aquariusBeauty, aquariusErotic, aquariusGold,
                piscesBusiness, piscesCommon, piscesLove, piscesCar, piscesHealth, piscesBeauty, piscesErotic, piscesGold);


        final var aries = horoWeekly.getAries();
        final var taurus = horoWeekly.getTaurus();
        final var gemini = horoWeekly.getGemini();

        final var ariesCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("aries")
                .text(aries.getCommon().getText().replace("\n", ""))
                .build();
        final var ariesBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("aries")
                .text(aries.getBusiness().getText().replace("\n", ""))
                .build();
        final var ariesLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("aries")
                .text(aries.getLove().getText().replace("\n", ""))
                .build();
        final var ariesCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("aries")
                .text(aries.getCar().getText().replace("\n", ""))
                .build();
        final var ariesHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("aries")
                .text(aries.getHealth().getText().replace("\n", ""))
                .build();
        final var ariesBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("aries")
                .text(aries.getBeauty().getText().replace("\n", ""))
                .build();
        final var ariesErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("aries")
                .text(aries.getErotic().getText().replace("\n", ""))
                .build();
        final var ariesGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("aries")
                .text(aries.getGold().getText().replace("\n", ""))
                .build();

        final var taurusCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("taurus")
                .text(taurus.getCommon().getText().replace("\n", ""))
                .build();
        final var taurusBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("taurus")
                .text(taurus.getBusiness().getText().replace("\n", ""))
                .build();
        final var taurusLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("taurus")
                .text(taurus.getLove().getText().replace("\n", ""))
                .build();
        final var taurusCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("taurus")
                .text(taurus.getCar().getText().replace("\n", ""))
                .build();
        final var taurusHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("taurus")
                .text(taurus.getHealth().getText().replace("\n", ""))
                .build();
        final var taurusBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("taurus")
                .text(taurus.getBeauty().getText().replace("\n", ""))
                .build();
        final var taurusErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("taurus")
                .text(taurus.getErotic().getText().replace("\n", ""))
                .build();
        final var taurusGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("taurus")
                .text(taurus.getGold().getText().replace("\n", ""))
                .build();


        final var geminiCommon = Horoscope.builder()
                .week(week)
                .type("common")
                .zodiacSign("gemini")
                .text(gemini.getCommon().getText().replace("\n", ""))
                .build();
        final var geminiBusiness = Horoscope.builder()
                .week(week)
                .type("business")
                .zodiacSign("gemini")
                .text(gemini.getBusiness().getText().replace("\n", ""))
                .build();
        final var geminiLove = Horoscope.builder()
                .week(week)
                .type("love")
                .zodiacSign("gemini")
                .text(gemini.getLove().getText().replace("\n", ""))
                .build();
        final var geminiCar = Horoscope.builder()
                .week(week)
                .type("car")
                .zodiacSign("gemini")
                .text(gemini.getCar().getText().replace("\n", ""))
                .build();
        final var geminiHealth = Horoscope.builder()
                .week(week)
                .type("health")
                .zodiacSign("gemini")
                .text(gemini.getHealth().getText().replace("\n", ""))
                .build();
        final var geminiBeauty = Horoscope.builder()
                .week(week)
                .type("beauty")
                .zodiacSign("gemini")
                .text(gemini.getBeauty().getText().replace("\n", ""))
                .build();
        final var geminiErotic = Horoscope.builder()
                .week(week)
                .type("erotic")
                .zodiacSign("gemini")
                .text(gemini.getErotic().getText().replace("\n", ""))
                .build();
        final var geminiGold = Horoscope.builder()
                .week(week)
                .type("gold")
                .zodiacSign("gemini")
                .text(gemini.getGold().getText().replace("\n", ""))
                .build();

        Collections.addAll(horoscopes,
                ariesBusiness, ariesCommon, ariesLove, ariesCar, ariesHealth, ariesBeauty, ariesErotic, ariesGold,
                taurusBusiness, taurusCommon, taurusLove, taurusCar, taurusHealth, taurusBeauty, taurusErotic, taurusGold,
                geminiBusiness, geminiCommon, geminiLove, geminiCar, geminiHealth, geminiBeauty, geminiErotic, geminiGold);

        return horoscopes;
    }
}
