package ru.eltgm.horoscope.service.ignio;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.eltgm.horoscope.models.HoroscopeSource;
import ru.eltgm.horoscope.models.ignio.day.HoroDay;
import ru.eltgm.horoscope.models.ignio.weekly.HoroWeekly;

import java.util.concurrent.CompletableFuture;

@Component
@RequiredArgsConstructor
public class IgnioHTTPBean {
    private final RestTemplate restTemplate;

    public CompletableFuture<HoroDay> getDayHoroscope(HoroscopeSource source) {
        final var horoDay = restTemplate.getForObject(source.getUrl(), HoroDay.class);

        return CompletableFuture.completedFuture(horoDay);
    }

    public CompletableFuture<HoroWeekly> getWeeklyHoroscope(HoroscopeSource source) {
        final var horoWeekly = restTemplate.getForObject(source.getUrl(), HoroWeekly.class);

        return CompletableFuture.completedFuture(horoWeekly);
    }
}
