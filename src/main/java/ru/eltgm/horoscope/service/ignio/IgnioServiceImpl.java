package ru.eltgm.horoscope.service.ignio;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.eltgm.horoscope.models.HoroscopeSource;
import ru.eltgm.horoscope.repository.HoroscopesRepository;
import ru.eltgm.horoscope.service.HoroscopeService;

@Service
@Async
@RequiredArgsConstructor
public class IgnioServiceImpl implements HoroscopeService {
    private final HoroscopesRepository horoscopesRepository;
    private final IgnioHTTPBean ignioHTTPBean;

    @Override
    public void updateDay(HoroscopeSource source) {
        ignioHTTPBean.getDayHoroscope(source).whenComplete((horoDay, throwable) -> {
            if (horoDay != null) {
                final var horoscopes = HoroscopeMapper.getHoroscopes(horoDay, source.getHoroscopeType());

                if (!horoscopes.isEmpty()) {
                    horoscopesRepository.saveAll(horoscopes);
                }
            }
        });
    }

    @Override
    public void updateWeekly(HoroscopeSource source) {
        ignioHTTPBean.getWeeklyHoroscope(source).whenComplete((horoWeekly, throwable) -> {
            if (horoWeekly != null) {
                final var weeklyHoroscopes = HoroscopeMapper.getWeeklyHoroscopes(horoWeekly);

                if (!weeklyHoroscopes.isEmpty()) {
                    horoscopesRepository.saveAll(weeklyHoroscopes);
                }
            }
        });
    }

    @Override
    public void deleteOldDay() {
        horoscopesRepository.deleteAllByDateNotNullAndWeekNull();
    }

    @Override
    public void deleteOldWeek() {
        horoscopesRepository.deleteAllByWeekNotNullAndDateNull();
    }
}


