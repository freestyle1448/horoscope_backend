package ru.eltgm.horoscope.service;

import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.HoroscopeSource;
import ru.eltgm.horoscope.models.Type;
import ru.eltgm.horoscope.models.User;

import java.util.List;

public interface AdminService {
    Page<Horoscope> getDayHoroscopes(String sign, String type, int page);

    Page<Horoscope> getWeeklyHoroscopes(String sign, String type, int page);

    Boolean saveHoroscope(Horoscope horoscope);

    Page<Type> getTypes(int page);

    List<Type> getAllTypes();

    void updateType(Type type);

    void createType(Type type);

    void deleteType(ObjectId id);

    Page<User> getUsers(int page);

    Page<HoroscopeSource> getSources(int page);

    Boolean updateSource(HoroscopeSource horoscopeSource);

    Boolean deleteSource(ObjectId id);

    Boolean saveSource(HoroscopeSource updatedSource);

    Boolean saveUser(User updateUser);
}
