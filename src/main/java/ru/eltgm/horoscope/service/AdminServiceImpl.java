package ru.eltgm.horoscope.service;

import lombok.RequiredArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.HoroscopeSource;
import ru.eltgm.horoscope.models.Type;
import ru.eltgm.horoscope.models.User;
import ru.eltgm.horoscope.repository.HoroscopeSourcesRepository;
import ru.eltgm.horoscope.repository.HoroscopesRepository;
import ru.eltgm.horoscope.repository.TypesRepository;
import ru.eltgm.horoscope.repository.UsersRepository;

import java.util.List;

@RequiredArgsConstructor
@Service
public class AdminServiceImpl implements AdminService {
    private final HoroscopesRepository horoscopesRepository;
    private final TypesRepository typesRepository;
    private final UsersRepository usersRepository;
    private final HoroscopeSourcesRepository horoscopeSourcesRepository;

    @Override
    public Page<Horoscope> getDayHoroscopes(String sign, String type, int page) {
        return horoscopesRepository.findAllByZodiacSignAndTypeAndDateNotNullAndWeekNull(
                sign, type, PageRequest.of(page, 10, Sort.by("date").descending()));
    }

    @Override
    public Page<Horoscope> getWeeklyHoroscopes(String sign, String type, int page) {
        return horoscopesRepository.findAllByZodiacSignAndTypeAndWeekNotNullAndDateNull(
                sign, type, PageRequest.of(page, 10, Sort.by("date").descending()));
    }

    @Override
    public Boolean saveHoroscope(Horoscope horoscope) {
        horoscopesRepository.save(horoscope);
        return true;
    }

    @Override
    public Page<Type> getTypes(int page) {
        return typesRepository.findAll(PageRequest.of(page, 10, Sort.by("isPremium")));
    }

    @Override
    public List<Type> getAllTypes() {
        return typesRepository.findAll();
    }

    @Override
    public void updateType(Type type) {
        typesRepository.save(type);
    }

    @Override
    public void createType(Type type) {
        typesRepository.save(type);
    }

    @Override
    public void deleteType(ObjectId id) {
        typesRepository.deleteById(id);
    }

    @Override
    public Page<User> getUsers(int page) {
        return usersRepository.findAll(PageRequest.of(page, 40, Sort.by("login")));
    }

    @Override
    public Page<HoroscopeSource> getSources(int page) {
        return horoscopeSourcesRepository.findAll(PageRequest.of(page, 10));
    }

    @Override
    public Boolean updateSource(HoroscopeSource horoscopeSource) {
        horoscopeSourcesRepository.save(horoscopeSource);

        return true;
    }

    @Override
    public Boolean deleteSource(ObjectId id) {
        horoscopeSourcesRepository.deleteById(id);

        return true;
    }

    @Override
    public Boolean saveSource(HoroscopeSource updatedSource) {
        horoscopeSourcesRepository.save(updatedSource);
        return true;
    }

    @Override
    public Boolean saveUser(User updateUser) {
        usersRepository.save(updateUser);
        return true;
    }
}
