package ru.eltgm.horoscope.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.eltgm.horoscope.exception.NotAllowedException;
import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.Type;
import ru.eltgm.horoscope.models.User;
import ru.eltgm.horoscope.models.UserType;
import ru.eltgm.horoscope.repository.HoroscopesRepository;
import ru.eltgm.horoscope.repository.TypesRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ClientServiceImpl implements ClientService {
    private final HoroscopesRepository horoscopesRepository;
    private final TypesRepository typesRepository;

    @Override
    public List<UserType> getAllTypesByDayType(User user, String dayType) {
        return typesRepository
                .findAllByDayType(dayType)
                .stream()
                .map(type -> {
                    if (user.getIsPremium()) {
                        return new UserType(type.getTypeName(), true,
                                type.getTypeNameRussian(), type.getDayType());
                    } else {
                        return new UserType(type.getTypeName(), !type.getIsPremium(),
                                type.getTypeNameRussian(), type.getDayType());
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<Horoscope> getAllAvailableHoroscopesBySign(User user, String sign) {
        final var types = typesRepository.findAll()
                .stream()
                .filter(type -> {
                    if (user.getIsPremium()) {
                        return true;
                    } else {
                        return !type.getIsPremium();
                    }
                })
                .map(Type::getTypeName)
                .collect(Collectors.toList());

        return horoscopesRepository.findAllByTypeInAndZodiacSign(types, sign);
    }

    @Override
    public List<Horoscope> getAllAvailableHoroscopes(User user) {
        final var types = typesRepository.findAll()
                .stream()
                .filter(type -> {
                    if (user.getIsPremium()) {
                        return true;
                    } else {
                        return !type.getIsPremium();
                    }
                })
                .map(Type::getTypeName)
                .collect(Collectors.toList());

        return horoscopesRepository.findAllByTypeIn(types);
    }

    @Override
    public Horoscope getHoroscopeBySignDayType(User user, String sign, String day, String horoscopeType) {
        boolean isAllowed = false;
        String dayType = "day";
        if (day.equals("WEEK")) {
            dayType = "week";
        }

        for (UserType type : getAllTypesByDayType(user, dayType)) {
            if (type.getTypeName().equals(horoscopeType)) {
                isAllowed = true;
                break;
            }
        }

        if (!isAllowed) {
            throw new NotAllowedException("Не досутпно для этого аккаунта!");
        }

        if (day.equals("WEEK")) {
            return horoscopesRepository.findByZodiacSignAndWeekIsNotNullAndType(sign, horoscopeType);
        }

        LocalDate horoscopeDate = LocalDate.now();
        if ("TOMORROW".equals(day)) {
            horoscopeDate = horoscopeDate.plusDays(1);
        } else if ("TOMORROW02".equals(day)) {
            horoscopeDate = horoscopeDate.plusDays(2);
        }

        return horoscopesRepository.findByZodiacSignAndDateAndType(sign, horoscopeDate.toString(), horoscopeType);
    }
}
