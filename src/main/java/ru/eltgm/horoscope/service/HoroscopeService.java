package ru.eltgm.horoscope.service;

import ru.eltgm.horoscope.models.HoroscopeSource;

public interface HoroscopeService {
    void updateDay(HoroscopeSource source);

    void updateWeekly(HoroscopeSource source);

    void deleteOldDay();

    void deleteOldWeek();
}
