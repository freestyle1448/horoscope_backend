package ru.eltgm.horoscope.service;

import ru.eltgm.horoscope.models.Horoscope;
import ru.eltgm.horoscope.models.User;
import ru.eltgm.horoscope.models.UserType;

import java.util.List;

public interface ClientService {
    List<UserType> getAllTypesByDayType(User user, String dayType);

    List<Horoscope> getAllAvailableHoroscopesBySign(User user, String sign);

    List<Horoscope> getAllAvailableHoroscopes(User user);

    Horoscope getHoroscopeBySignDayType(User user, String sign, String day, String horoscopeType);
}
